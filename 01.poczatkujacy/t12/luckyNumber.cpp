//http://codeforces.com/problemset/problem/110/A
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    long long number;
    int digit;
    int result_digits;

    result_digits = 0;
    cin >> number;

    while(number > 0)
    {
        digit = number % 10;
        if ((digit == 4) || (digit ==7))
            result_digits++;
        number = number / 10;
    }

    if ((result_digits == 4) || (result_digits == 7))
        cout << "YES" << "\n";
    else
        cout << "NO" << "\n";

    return 0;
}
