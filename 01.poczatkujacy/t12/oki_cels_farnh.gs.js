function changeCelsjusFahrenheit(temp_celsjus) {
  var temp_fahrenheit;
  temp_fahrenheit = 32 + (temp_celsjus*9)/5;
  temp_fahrenheit = Math.round(temp_fahrenheit);
  return temp_fahrenheit;
}

function advice (temp_celsjus) {
 if ( temp_celsjus < 0 )
    return 'Marzn� uszy!';
 if ( temp_celsjus == 0 )
    return '�lisko!';
 if ( temp_celsjus < 10 )
    return 'Rzesko...';
 if ( temp_celsjus < 20 )
    return 'We� sweter';
 if ( temp_celsjus < 30 )
    return 'Ciep�o i mi�o';
 return 'SKWAR!!!';
}

function primary (my_number) {
  var square_root;
  var residual;

  if (my_number <= 1) return 'Liczba nie jest ani pierwsza ani z�o�ona';
  if (my_number == 2) return 'Liczba pierwsza';

  square_root = Math.sqrt(my_number);
  square_root = Math.ceil (square_root);
  for (i=2; i<=square_root; ++i) {
   residual = my_number % i;
   if ( residual == 0 )
      return 'Liczba z�o�ona';
  }
  return 'Liczba pierwsza';
}
