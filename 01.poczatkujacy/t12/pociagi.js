function odlegloscA (s, v1, v2)
{
    var s1;
    s1 = v1 * s / (v1 + v2);
    s1 = Math.round(s1);
    return s1;
}

function odlegloscB (s, v1, v2)
{
    var s2;
    s2 = v2 * s / (v1 + v2);
    s2 = Math.round(s2);
    return s2;
}
