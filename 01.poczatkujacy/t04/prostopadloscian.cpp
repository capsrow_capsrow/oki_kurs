#include <bits/stdc++.h>
using namespace std;

int main (){
    int dlugosc, szerokosc, wysokosc; //wymiary prostopadloscianu
    int pole_powierzchni_prostopadloscianu;
    int objetosc_prostopadloscianu;

    cin >> dlugosc >> szerokosc >> wysokosc;

    pole_powierzchni_prostopadloscianu = 2 * (dlugosc*szerokosc + dlugosc*wysokosc + szerokosc*wysokosc);
    objetosc_prostopadloscianu = dlugosc * szerokosc * wysokosc;

    cout << objetosc_prostopadloscianu << "\n";
    cout << pole_powierzchni_prostopadloscianu << "\n";
}
