#include <bits/stdc++.h>
using namespace std;

int main (){
    int nr_of_participants;
    int kth_place;
    int point;
    int sum;

    cin >> nr_of_participants >> kth_place;

    int points[nr_of_participants];

    for (int i = 1; i <= nr_of_participants; ++i)
        cin >> points[i];


    sum = 0;
    for (int i = 1; i <= nr_of_participants; ++i)
    {
        if (points[i] > 0)
        {
         if (points[i] >= points[kth_place])
             sum++;
        }
    }

    cout << sum << "\n";

    return 0;
}
