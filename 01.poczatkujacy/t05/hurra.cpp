#include <bits/stdc++.h>
using namespace std;

int main (){
    int liczba;

    cin >> liczba;

    for (int i = 1; i <= liczba; ++i)
    {
        if (i % 77 == 0)
        {
            cout << "Wiwat!" << "\n";
            continue;
        }
        if (i % 7 == 0)
        {
            cout << "Hurra!" << "\n";
            continue;
        }
        if (i % 11 == 0)
        {
            cout << "Super!" << "\n";
            continue;
        }
        else
            cout << i << "\n";
    }

    return 0;
}
