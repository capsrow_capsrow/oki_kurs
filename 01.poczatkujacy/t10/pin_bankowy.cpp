#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba[10];

    for (int i = 0; i < 10; ++i)
        cin >> liczba[i];

    cout << liczba[6] << " ";
    cout << liczba[3] << "\n";
    cout << liczba[2] - liczba[7] << " ";
    cout << liczba[8] - liczba[5] << "\n";

    return 0;
}
