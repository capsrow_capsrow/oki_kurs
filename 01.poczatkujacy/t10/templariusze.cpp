#include <bits/stdc++.h>
using namespace std;

int srednia(int *tablica, int liczba_elementow)
{
    int wynik;

    for (int i = 0; i < liczba_elementow; i++)
        wynik += tablica[i];

    wynik = wynik / liczba_elementow;

    return wynik;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int skrzynie[13];
    int waga_1, waga_2;

    for (int i = 0; i < 13; i++)
        cin >> skrzynie[i];

    cin >> waga_1;
    cin >> waga_2;

    skrzynie[0] = skrzynie[0] + waga_1;
    skrzynie[7] = skrzynie[7] + waga_2;

    cout << skrzynie[0] << " ";
    cout << skrzynie[5] << " ";
    cout << skrzynie[12] << "\n";
    cout << srednia(skrzynie, 13) << "\n";

    return 0;
}
