#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba_ocen;

    cin >> liczba_ocen;

    int oceny[liczba_ocen];
    int suma_prefix[7];

    for (int i = 0; i < 7; ++i)
        suma_prefix[i] = 0;

    for (int i = 0; i < liczba_ocen; ++i)
    {
        cin >> oceny[i];
        suma_prefix[oceny[i]]++;
    }

    for (int i = 1; i < 7; ++i)
    {
        cout << suma_prefix[i];
        if (i < 6)
            cout << " ";
        else
            cout << "\n";
    }


    return 0;
}

