#include <bits/stdc++.h>
using namespace std;

int main (){
    int rok;
    int czyprzez100, czyprzez400, czyprzez4;

    cin >> rok;

    czyprzez100 = rok % 100;
    czyprzez400 = rok % 400;
    czyprzez4   = rok % 4;

    if (czyprzez100 == 0)
    {
        if (czyprzez400 == 0)
        {
            cout << "TAK" << "\n";
            return 0;
        }
        else
        {
            cout << "NIE" << "\n";
            return 0;
        }
    }

    if (czyprzez4 == 0)
        cout << "TAK" << "\n";
    else
        cout << "NIE" << "\n";

    return 0;
}
