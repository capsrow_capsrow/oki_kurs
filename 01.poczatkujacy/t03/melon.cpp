#include <bits/stdc++.h>
using namespace std;

int main (){
    int weight, reminder;

    cin >> weight;

    if (weight <= 2)
    {
        cout << "NO" << "\n";
        return 0;
    }

    reminder = weight % 2;

    if (reminder == 0)
        cout << "YES" << "\n";
    else
        cout << "NO" << "\n";

    return 0;
}
