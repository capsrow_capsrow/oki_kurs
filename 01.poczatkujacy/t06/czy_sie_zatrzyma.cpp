#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int odp;
    long long int n;
    cin >> n;

    while (n > 1)
    {
        if (n % 2 == 0)
        {
            n = n / 2;
            if (n == 1)
            {
                odp = 1;
                n = 0; //wyjdz z tej petli
            }
        }
        else
        {
            odp = 0;
            n = 0; //wyjdz z tej petli
        }
    }

    if (odp == 1)
        cout << "TAK" << "\n";
    if (odp == 0)
        cout << "NIE" << "\n";

    return 0;
}
