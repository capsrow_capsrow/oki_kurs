#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int dlugosc_przedzialu_male_litery, liczba_liter_przedzial_FT;
    char najmniejsza_mala_litera, najwieksza_mala_litera;

    int w;
    int i;
    char znak;

    dlugosc_przedzialu_male_litery = 0;
    liczba_liter_przedzial_FT = 0;
    najmniejsza_mala_litera = 122;
    najwieksza_mala_litera = 0;

    cin >> w;

    for (i = 0; i < w; ++i)
    {
        cin >> znak;

        if ((znak >= 'a') && (znak <= 'z'))
        {
            if (znak < najmniejsza_mala_litera)
                najmniejsza_mala_litera = znak;
            if (znak >= najwieksza_mala_litera)
                najwieksza_mala_litera = znak;

            dlugosc_przedzialu_male_litery = (int) najwieksza_mala_litera - (int) najmniejsza_mala_litera + 1;
            continue;
        }

        if ((znak >= 'F') && (znak <= 'T'))
        {
            liczba_liter_przedzial_FT++;
            continue;
        }
    }

    if (dlugosc_przedzialu_male_litery > 0)
    {
        cout << "Przedzial w ktorym mieszcza sie male litery ma dlugosc " << dlugosc_przedzialu_male_litery << "\n";
        cout << "Najmniejsza leksykograficznie mala litera to " << najmniejsza_mala_litera << "\n";
        cout << "Kod najmniejszej leksykograficznie malej litery " << (int) najmniejsza_mala_litera << "\n";
    }
    else
        cout << "Brak malych liter" << "\n";


   if (liczba_liter_przedzial_FT > 0)
        cout << "Liczba duzych liter od F do T wynosi " << liczba_liter_przedzial_FT << "\n";
    else
        cout << "Brak duzych liter w przedziale F do T" << "\n";

    return 0;
}
