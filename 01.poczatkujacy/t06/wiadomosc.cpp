#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba_malych_liter, liczba_duzych_liter, liczba_innych_znakow;
    char najwieksza_litera = -1;
    int w;
    int i;
    char znak;

    liczba_malych_liter = 0;
    liczba_duzych_liter = 0;
    liczba_innych_znakow = 0;

    cin >> w;

    for (i = 0; i < w; ++i)
    {
        cin >> znak;

        if ((znak >= 'a') && (znak <= 'z'))
        {
            liczba_malych_liter++;
            continue;
        }
        else if ((znak >= 'A') && (znak <= 'Z'))
        {
            liczba_duzych_liter++;
            if (znak > najwieksza_litera)
                najwieksza_litera = znak;
            continue;
        }
        else
            liczba_innych_znakow++;
    }

    cout << "Liczba malych liter wynosi " << liczba_malych_liter << "\n";
    cout << "Liczba duzych liter wynosi " << liczba_duzych_liter << "\n";
    cout << "Liczba pozostalych znakow wynosi " << liczba_innych_znakow << "\n";
    if (liczba_duzych_liter > 0)
    {
        cout << "Najwieksza leksykograficznie duza litera to " << najwieksza_litera << "\n";
        cout << "Kod najwiekszej leksykograficznie duzej litery to " << (int) najwieksza_litera << "\n";
    }

    return 0;
}
