#include <iostream>
#include <string>
using namespace std;

int suma_dwoch_ostatnich_i_osatniej(int liczba)
{
    int ostatnia;
    int dwie_ostatnie;
    int wynik;

    ostatnia = liczba % 10;
    dwie_ostatnie = liczba % 100;

    wynik = dwie_ostatnie + ostatnia;

    return wynik;
}

void dwie_ostatnie_cyfry (int liczba, int &cyfra_1, int &cyfra_2)
{
    cyfra_1 = liczba % 10;
    liczba = liczba / 10;

    cyfra_2 = liczba % 10;
}

int main() {

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba_wysp;
    char polecenie;
    int liczba;
    int cyfra_1, cyfra_2;

    int suma_2_ostatnich_i_ostaniej;

    cin >> liczba_wysp;

    for (int i = 0; i < liczba_wysp; i++)
    {
        cin >> polecenie;
        cin >> liczba;

        if (polecenie == 'd')
        {
            suma_2_ostatnich_i_ostaniej = suma_dwoch_ostatnich_i_osatniej(liczba);
            cout << suma_2_ostatnich_i_ostaniej << " ";
            continue;
        }
        if (polecenie == 'p')
        {
            suma_2_ostatnich_i_ostaniej = suma_dwoch_ostatnich_i_osatniej(liczba);
            cout << suma_2_ostatnich_i_ostaniej + 3 << " ";
            continue;
        }
        if (polecenie == 'm')
        {
            suma_2_ostatnich_i_ostaniej = suma_dwoch_ostatnich_i_osatniej(liczba);
            cout << suma_2_ostatnich_i_ostaniej - 1 << " ";
            continue;
        }
        if (polecenie == 's')
        {
            dwie_ostatnie_cyfry(liczba, cyfra_1, cyfra_2);
            cout << cyfra_1 + cyfra_2 << " ";
            continue;
        }
        if (polecenie == 'z')
        {
            dwie_ostatnie_cyfry(liczba, cyfra_1, cyfra_2);
            cout << cyfra_1 * cyfra_2 << " ";
            continue;
        }
    }
}
