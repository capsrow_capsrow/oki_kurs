#include <iostream>
#include <string>
using namespace std;

int mini(int liczba_1, int liczba_2)
{
    if (liczba_1 > liczba_2)
        return liczba_2;
    else
        return liczba_1;
}

int main() {

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba_1, liczba_2, liczba_3;
    int minimum;

    cin >> liczba_1;
    cin >> liczba_2;
    cin >> liczba_3;

    minimum = mini(liczba_1, liczba_2);
    minimum = mini(minimum, liczba_3);

    cout << minimum;

}
