#include <iostream>
#include <limits>
#include <string>
using namespace std;

string usun_spacje(string napis)
{
    string wyjscie;

    for (int i = 0; i < napis.length(); i++)
    {
        if (napis[i] != ' ')
            wyjscie += napis[i];
    }

    return wyjscie;
}

string kompresja(string napis)
{
    char prev;
    string wyjscie;

    prev = napis[0];
    wyjscie += napis[0];
    for (int i = 1; i < napis.length(); i++)
    {
        if (napis[i] != prev)
            wyjscie += napis[i];
        prev = napis[i];
    }

    return wyjscie;
}

int main() {

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string we;

    while (getline(cin, we))
    {
        if (we == "")
            break;

        we = usun_spacje(we);
        if (we != "")
            we = kompresja(we);

        cout << we << "\n";
    }
}
