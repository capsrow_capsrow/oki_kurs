#include <iostream>
#include <string>
#include <cstdio>

using namespace std;

//http://usaco.org/index.php?page=viewproblem2&cpid=507



int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    freopen("cowroute.in", "r", stdin);
    freopen("cowroute.out", "w", stdout);

    int a, b, n;
    int cost;
    int no_of_cities;
    int city;
    int a_found;
    int result;
    #define INF (int)1e9

    cin >> a;
    cin >> b;
    cin >> n;

    result = INF;
    for (int i = 0; i < 2*n; i++)
    {
        cost = 0;
        no_of_cities = 0;
        a_found = 0;

        cin >> cost;
        cin >> no_of_cities;

        for (int j = 0; j < no_of_cities; j++)
        {
            city = 0;
            cin >> city;

            if (city == a)
                a_found = 1;

            if (a_found == 1 && city == b)
                result = min(result, cost);
        }
    }

    if ((result == INF))
        cout << -1 << "\n";
    else
        cout << result << "\n";

    return 0;
}
