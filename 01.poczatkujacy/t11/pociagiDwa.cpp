#include <bits/stdc++.h>
using namespace std;

double odlegloscA (double s, double v1, double v2)
{
    double s1;
    s1 = v1 * s / (v1 + v2);
    return s1;
}

double odlegloscB (double s, double v1, double v2)
{
    double s2;
    s2 = v2 * s / (v1 + v2);
    return s2;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    double s;
    double v1, v2;

    double s1 = 0;
    double s2 = 0;

    cin >> s;
    cin >> v1;
    cin >> v2;

    s1 = odlegloscA(s, v1, v2);
    s2 = odlegloscB(s, v1, v2);

    cout << fixed << setprecision(2) << s1 << " " << s2 << "\n";

    return 0;
}
