#include <bits/stdc++.h>
#include <math.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    double r;
    double pole, obwod;

    cin >> r;

    pole = M_PI * r * r;
    obwod = 2 * M_PI * r;

    cout << fixed << setprecision(3);
    cout << pole << "\n";
    cout << obwod << "\n";

    return 0;
}
