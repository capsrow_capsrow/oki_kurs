#include <bits/stdc++.h>
using namespace std;

int compareLexicographically (string s1, string s2)
{
    int result = 0;

    for (int i = 0; i < s1.size(); ++i)
    {
        if (toupper(s1[i]) < toupper(s2[i]))
        {
            result = -1;
            return result;
        }
        if (toupper(s1[i]) > toupper(s2[i]))
        {
            result = 1;
            return result;
        }
    }

    return result;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int no_of_strings = 2;
    int result = 0;

    string sTable[no_of_strings];

    for (int i = 0; i < no_of_strings; ++i)
        cin >> sTable[i];

    result = compareLexicographically(sTable[0], sTable[1]);

    cout << result << "\n";

    return 0;
}
