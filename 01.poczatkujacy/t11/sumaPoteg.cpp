#include <bits/stdc++.h>
using namespace std;

double potega(double podstawa, double wykladnik)
{
    double potega;

    potega = 1;

    if (wykladnik == 0)
        potega = 1;
    else
    {
        for (int i = 1; i <= wykladnik; ++i)
            potega *= podstawa;
    }

    return potega;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n, m;
    cin >> n;
    cin >> m;

    double a[n];
    double suma = 0;

    for (int i = 0; i < n; ++i)
        cin >> a[i];

    for (int i = 0; i < n; ++i)
        suma += potega(a[i], m);

    cout << fixed << setprecision(3);
    cout << suma << "\n";

    return 0;
}
