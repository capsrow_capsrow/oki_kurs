//https://szkopul.edu.pl/c/test1a2/problemset/problem/lokata/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int p, q;
    cin >> p;
    cin >> q;


    int kwota_na_lokacie = p;
    int miesiac = 0;
    int odsetki;

    while (kwota_na_lokacie < 2 * p)
    {
        miesiac++;
        odsetki = (kwota_na_lokacie * q) / 1200;
        if (odsetki == 0)
        {
            cout << "NIE" << "\n";
            break;
        }
        else
        {
            kwota_na_lokacie += odsetki;
            cout << miesiac << " " << kwota_na_lokacie << " " << odsetki << "\n";
        }
    }

    return 0;
}
