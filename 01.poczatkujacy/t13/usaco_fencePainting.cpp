//http://usaco.org/index.php?page=viewproblem2&cpid=567
#include <iostream>
#include <algorithm>
#include <cstdio>

using namespace std;

//Farmer: from 7 to 10
//Cow: from 4 to 8
//Farmer
// 0 0 0 0 0 0 0 1 1 1
//Cow
// 0 0 0 0 1 1 1 1 0 0
//finnaly:
// 0 0 0 0 1 1 1 1 1 1
// 1 2 3 4 5 6 7 8 9 10

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    freopen("paint.in", "r", stdin);
    freopen("paint.out", "w", stdout);

    int paint_interval[4];
    //Farmer
    for (int i = 0; i < 2; ++i)
        cin >> paint_interval[i];

    //Cow
    for (int i = 2; i < 4; ++i)
        cin >> paint_interval[i];



    int fenceRange = 0;
    if (paint_interval[1] < paint_interval [2])
    {
        fenceRange = paint_interval[1] - paint_interval[0];
        fenceRange += paint_interval[3] - paint_interval[2];
    }
    else if ((paint_interval[0] > paint_interval [2]) && (paint_interval[0] > paint_interval [3]))
    {
        fenceRange = paint_interval[1] - paint_interval[0];
        fenceRange += paint_interval[3] - paint_interval[2];
    }
    else
    {
        //sort table of paint intervals
        sort(paint_interval, paint_interval + 4);
        fenceRange = paint_interval[3] - paint_interval[0];
    }


    cout << fenceRange << "\n";

    return 0;
}
