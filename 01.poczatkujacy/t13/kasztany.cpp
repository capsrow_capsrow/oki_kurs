//https://szkopul.edu.pl/problemset/problem/kBBkITfyG3bL55s3QiAbQ4zJ/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);


    int liczba_dzieci;
    cin >> liczba_dzieci;

    int tabela_out[liczba_dzieci];
    for (int i = 0; i < liczba_dzieci; ++i)
        tabela_out[i] = 0;

    int dni;
    for (int i = 0; i < liczba_dzieci; ++i)
    {
        dni = 0;
        cin >> dni;

        int kasztany[dni];

        for (int j = 0; j < dni; ++j)
        {
            cin >> kasztany[j];
            tabela_out[i] += kasztany[j];
        }
    }

    for (int i = 0; i < liczba_dzieci; ++i)
        cout << tabela_out[i] << "\n";

    return 0;
}
