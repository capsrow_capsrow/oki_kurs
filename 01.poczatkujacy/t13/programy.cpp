//https://szkopul.edu.pl/problemset/problem/iO2JdspPtx_KMGhBTCbLnKYe/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

//tablica zadeklarowana globalnie moze byc wieksza niz tablica zadeklarowana w main(), albo lokalnie
//ablica zadeklarowana globalnie jest pd razu wyzerowana
const int max_elementow = 1e6 + 7; //1 0000 0000 + 7 elmentow
int rozmiar_programow[max_elementow];
int pojemnosc_plyt[max_elementow];

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int liczba_programow;
    cin >> liczba_programow;
    for (int i = 0;i < liczba_programow; ++i)
        cin >> rozmiar_programow[i];

    int liczba_plyt;
    cin >> liczba_plyt;
    for (int i = 0; i < liczba_plyt; ++i)
        cin >> pojemnosc_plyt[i];


    //sortowanie tablicy rozmiar_programow
    //1-szy parametr: wskaznik na tavlicze, ktora ma zostac posortowana, czyli pierwsza komorka tej tablicy
    //1-szy parametr ----> wskazujemy gdzie jest pierwszy element od ktorego zaczynamy sortowanie
    //2-gi  parametr ----> wskazujemy gdzie jest element od ktoreho MA NIE SORTOWAC, czyli pierwszy ktory nie sortuj
    sort(rozmiar_programow, rozmiar_programow + liczba_programow);

    sort(pojemnosc_plyt, pojemnosc_plyt + liczba_plyt);

    int numer_programu = 0;
    int numer_plyty = 0;
    int ile_przeniesionych = 0;

    while (true){ //petla nieskoncaona
        if (numer_programu >= liczba_programow) //przerwij gdy przeiterujesz sie po calej tablicy rozmiar_programow[]
            break;
        if (numer_plyty >= liczba_plyt)  //przerwij rowniez gdy przeiterujesz sie po calej tablicy pojemnosc_plyt[]
            break;
        if (rozmiar_programow[numer_programu] <= pojemnosc_plyt[numer_plyty]){
            ++ile_przeniesionych;
            ++numer_programu;
            ++numer_plyty;
            continue;   //kontynuuj p�tl�
        }
        //nei moglismy zmienic programu na plycie
        ++numer_plyty;
    }

    cout << ile_przeniesionych << "\n";


    return 0;
}
