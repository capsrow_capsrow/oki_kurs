//zadanie: https://szkopul.edu.pl/problemset/problem/sg/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

//wywolanie programu:
//18 8 1 18 36 9 3 1 5 16 16 42 15 23 5 12 26 9 28 33 15 11 a
// while (cin >> liczba) -> dopoki prawda, 'a' nie jest intem, zatem zostanie zwrocony blad i to przerwie petle while

void WypiszElementyWektora (const vector <int> &v, int indeks_poczatkowy, int ilosc_elementow) //&v - aby nie tobic kopii duzych wektorow, przekazujemy referencje
{
    int indeks_koncowy;

    indeks_koncowy = indeks_poczatkowy + ilosc_elementow;

    for (int i = indeks_poczatkowy; i < indeks_koncowy; ++i)
    {
        if ( i == (int) v.size()) //jezeli doszedlem do konca wektora
            break;                //przerywamy
        cout << v[i] << " ";
    }

    cout << "\n";
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    vector <int> konta;
    int liczba;
    int iLiczbaKlientow;
    int indeks_pocz;
    int indeks_polowa;

    while (cin >> liczba)
        konta.push_back(liczba);


    iLiczbaKlientow = konta.size();

    sort(konta.begin(), konta.end());


    cout << iLiczbaKlientow << "\n";    //ilosc wyczytanych liczb

    WypiszElementyWektora(konta, 0, 5); //5 liczb uporz�dkowanych rosnaco, ktore s� najmniejszymi wartosciami na wejsciu

    indeks_pocz = iLiczbaKlientow - 10;
    if (indeks_pocz < 0) indeks_pocz= 0;
    WypiszElementyWektora(konta, indeks_pocz, 10);//10 liczb uporz�dkowanych rosnaco, ktore s� najwiekszymi wartosciami na wejsciu

    indeks_polowa = (iLiczbaKlientow - 1) / 2;
    WypiszElementyWektora(konta, indeks_polowa, 20);//20 liczb uporzadkowanych rosnaco, poczawszy od liczby srodkowej
                                                    //jesli ilosc liczb jest parzysta zaczynamy, od indeksu rownego polowie ilosci liczb

    return 0;
}
