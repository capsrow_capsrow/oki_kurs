#include <bits/stdc++.h>
#include <string.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string phrase;
    cin >> phrase;

    if (phrase[0] >= 'a' || phrase[0] >= 'z')
        phrase[0] -= 32;

    cout << phrase;
}
