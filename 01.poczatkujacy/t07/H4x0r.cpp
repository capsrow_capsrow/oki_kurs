#include <bits/stdc++.h>
#include <string.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string phrase;
    cin >> phrase;

    for (int i = 0; i < phrase.length(); i++)
    {
        if (phrase[i] == 'a')
            phrase[i] = '4';
        else if (phrase[i] == 'e')
            phrase[i] = '3';
        else if (phrase[i] == 'i')
            phrase[i] = '1';
        else if (phrase[i] == 'o')
            phrase[i] = '0';
        else if (phrase[i] == 's')
            phrase[i] = '5';
    }

    cout << phrase;
}
