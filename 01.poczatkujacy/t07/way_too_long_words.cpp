#include <bits/stdc++.h>
#include <string.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    string phrase;
    int ph_length;
    string result;

    cin >> n;

    for (int i = 0; i < n; i++)
    {
        cin >> phrase;
        ph_length = phrase.length();

        if (ph_length <= 10)
        {
            cout << phrase << "\n";
            continue;
        }

        result = "";
        result += phrase[0] + to_string(ph_length - 2) + phrase[ph_length - 1];

        cout << result << "\n";
    }
}
