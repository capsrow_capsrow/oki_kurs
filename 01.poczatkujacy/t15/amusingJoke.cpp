//task: http://codeforces.com/problemset/problem/141/A

#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string sFirstText, sSecondText, sThirdText;

    cin >> sFirstText >> sSecondText >> sThirdText;

    int iFirstPass;
    int iSecondPass;
    int iThirdPass;

    iFirstPass = sFirstText.length();
    iSecondPass = sSecondText.length();
    iThirdPass = sThirdText.length();

    if ((iFirstPass + iSecondPass) != iThirdPass)
    {
        cout << "NO" << "\n";
        return 0;
    }

    bool passFirst[iFirstPass];
    bool passSecond[iSecondPass];
    bool passThird[iThirdPass];

    for (int i = 0; i < sFirstText.length(); ++i)
        passFirst[i] = 0;

    for (int i = 0; i < sSecondText.length(); ++i)
        passSecond[i] = 0;

    for (int i = 0; i < sThirdText.length(); ++i)
        passThird[i] = 0;

    for (int i = 0; i < sThirdText.length(); ++i)
    {
        for (int j = 0; j < sFirstText.length(); ++j)
        {
            if ((sFirstText[j] == sThirdText[i]) && (passFirst[j] == 0) && (passThird[i] == 0))
            {
                passFirst[j] = 1;
                passThird[i] = 1;
                --iFirstPass;
                --iThirdPass;
            }
        }
    }

    for (int i = 0; i < sThirdText.length(); ++i)
    {
        for (int j = 0; j < sSecondText.length(); ++j)
        {
            if ((sSecondText[j] == sThirdText[i]) && (passSecond[j] == 0) && (passThird[i] == 0))
            {
                passSecond[j] = 1;
                passThird[i] = 1;
                --iSecondPass;
                --iThirdPass;
            }
        }
    }

    if (iThirdPass == 0)
        cout << "YES" << "\n";
    else
        cout << "NO" << "\n";

    return 0;
}
