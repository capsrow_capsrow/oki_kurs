//zadanie: https://szkopul.edu.pl/problemset/problem/A859_vuqciUBllN7vf9w2NYL/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int h, m, s;

    cin >> h >> m >> s;

    s = s + 1;

    if (s == 60)
    {
        s = 0;
        m = m + 1;
    }

    if (m == 60)
    {
        m = 0;
        h = h + 1;
    }

    if (h == 24)
        h = 0;

    if (h < 10)
        cout << '0' << h << ':';
    else
        cout << h << ':';

    if (m < 10)
        cout << '0' << m << ':';
    else
        cout << m << ':';

    if (s < 10)
        cout << '0' << s << ':';
    else
        cout << s << ':';


    return 0;
}
