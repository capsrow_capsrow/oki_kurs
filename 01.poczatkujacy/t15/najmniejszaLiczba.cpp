//tresc zadania: https://szkopul.edu.pl/problemset/problem/yOvtNVzBXZuw0QkrppTB2MW-/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int a[4];//wczytywane liczby

    for (int i = 0; i < 3; ++i)
        cin >> a[i];

    sort(a, a + 3);

    //po posortowaniu
    //gdy druga cyfra jest zerem, to znaczy ze pierwsza cyfra tez jest zerem
    if (a[1] == 0)
    {
        cout << a[2] << "00" << "\n";
        return 0;
    }

    //po posortowaniu
    //gdy pierwsza cyfra jest zerem, i byc moze pierwsza cyfra jest zerem albo nie jest zerem
    if (a[0] == 0)
    {
        cout << a[1] << "0" << a[2] << "\n";
        return 0;
    }

    //w przeciwnym razie
    cout << a[0] << a[1] << a[2] << "\n";

    return 0;
}
