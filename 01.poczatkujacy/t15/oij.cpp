//tresc zadania: https://szkopul.edu.pl/problemset/problem/WawC7bmwUyPKAG25CCKdFiCP/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string sNapis;
    char cZnak;
    int iLitery, iSekwencje;


    cin >> sNapis;

    iLitery = 0;
    iSekwencje = 0;

    for (int i = 0; i < sNapis.length(); ++i)
    {
        cZnak = sNapis[i];
        switch (cZnak)
        {
            case 'o':   if (iLitery == 0) ++iLitery;
                        break;
            case 'i':   if (iLitery == 1) ++iLitery;
                        break;
            case 'j':   if (iLitery == 2) ++iLitery;
                        break;
        }

            if (iLitery == 3)
            {
                iLitery = 0;
                ++iSekwencje;
            }
    }

    if (iSekwencje > 0)
        cout << iSekwencje << "\n";
    else
        cout << "NIE" << "\n";

    return 0;
}
