#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int main()
{
    int maxX;
    int maxY;
    cin >> maxX >> maxY; cin.ignore();

    string tabRow[maxY+1];
    string sOutput[maxY+1];

    for (int y = 0; y < maxY; y++)
    {
        cin >> tabRow[y]; cin.ignore();
        cerr << "tabRow[" << y << "] = " << tabRow[y] << endl;
    }

    string side;
    cin >> side; cin.ignore();
    cerr << "side " << side << endl;

    int startX = 0, startY = 0;
    char direction = 0;
    //where is startX and startY point
    for (int y = 0; y < maxY; y++)
    {
        for (int x = 0; x < maxX; x++)
        {
            if ((tabRow[y][x] == '<') || (tabRow[y][x] == '>') || (tabRow[y][x] == '^') || (tabRow[y][x] == 'v'))
            {
                startX = x;
                startY = y;
                direction = tabRow[y][x];
                cerr << "startX = " << x << "; startY = " << y << "; direction = " << direction << endl;
                tabRow[y][x] = 0;
            }
        }
    }


    //kolejnosc ruchu dla side = Left
    //L (clockwise):     rigt,     down,     left,      up
    //                   (x+1, y)  (x, y+1)  (x-1, y)   (x, y -1)
    //kolejnsc ruchu dla  side = Right
    //R (rev-clockwise): down,     right,    up,        left
    //                   (x, y+1)  (x+1, y)  (x, y -1)  (x-1, y)
    for (int y = 0; y < maxY; y++)
    {

        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

        sOutput[y] = "";

        for (int x = 0; x < maxX; x++)
        {
            if (tabRow[y][x] == '#')
            {
                sOutput[y] += "#";
                //cerr << "sOutput[" << y << "][" << x << "] = " << sOutput << endl;
                continue;
            }
        }

        cout << "#####" << endl;
    }
}
