//https://szkopul.edu.pl/problemset/problem/koszulki/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;//liczba koszulek do wyslania
    int x;//max wartosc, od ktorej nie moze byc wieksza wartosc koszulek w jednej paczce
    const int max_kp = 2;//maksymalna liczba koszulek znajdujaca sie w jednej paczce
    const int max_kraje = 100;//maksymalna liczba krajow jaka moze sie pojawic na wejsciu
    int liczbaPaczek = 0;

    cin >> n;
    cin >> x;

    //wczytanie danych
    int koszulki[n], koszulki_kraje[n];
    int tmp[n];
    //czy koszulka o podanym indeksie zostala juz zapakowana czy nie zostala zapakowana
    int czyZapakowana[n];

    for (int i = 0; i < n; ++i)
        cin >> koszulki[i] >> koszulki_kraje[i];

    //maksymalnie 100 razy, bo 100 kraj�w
    for (int i = 1; i <= max_kraje; ++i)
    {
        //dla kazdego numeru kraju zeruje tablice tmp[j]
        //oraz czyZapakowana[j]
        for (int j = 0; j < n; ++j)
        {
            tmp[j] = 0;
            czyZapakowana[j] = 0;
        }


        int liczba_danych = 0;
        for (int j = 0; j < n; ++j)
        {
            //dla kolejnych krajow przepisuje wartosci koszulek do koszulki_tmp[j]
            if (koszulki_kraje[j] == i)
            {
                tmp[liczba_danych] = koszulki[j];
                ++liczba_danych;                //tyle koszulek dla danego kraju
            }

        }

        //sortuje tablice z warosciami koszulek dla danego kraju
        //sortowanie malejace: od najwiekszego do najmniejszego
        sort(tmp, tmp + liczba_danych, greater_equal <int> ());

        //gdy juz pakiet dla danego kraju jest posortowany
        //to dla kazdego pakietu n elemetowego obliczana jest liczba pudelek na koszulki
        //pomijane sa elmenty zerowe w pakiecie
        //algorytm:
        //1. wez najwieksza jeszcze niezapakowana koszulke dla kraju i dodaj najmniejsza jeszcze niezapakowana
        //a. jezeli suma wieksza od x, to zapakuj tylko najwieksza, po zapakowaniu oznacza czyZapakowana = 1, powieksz liczbaPaczek o +1
        //b. jezeli suma mniejsza od x,to zapakuj obie koszulki, po zapakowaniu obie oznacz czyZapakowana = 1, powieksz liczbaPaczek o +1
        //2. wykonuj krok (1) az wszystkie koszulki dla kraju beda oznaczone jako czyZapakowana = 1
        //3. wykonaj kroki (1) i (2) dla wszystkich krajow

        int j = liczba_danych;
        int poczatek = 0, koniec = liczba_danych - 1;
        while (j--)
        {
            if (((tmp[poczatek] + tmp[koniec]) <= x) && (czyZapakowana[poczatek] == 0) && (czyZapakowana[koniec] == 0) && (tmp[koniec] != 0))
            {
                //cout << "if 1: koszulki poczatek [" << poczatek << "] = " << tmp[poczatek] << "\n";
                //cout << "if 1: koszulki koniec [" << koniec << "] = " << tmp[koniec] << "\n";
                czyZapakowana[poczatek] = 1;
                czyZapakowana[koniec] = 1;
                liczbaPaczek++;
                poczatek++;
                koniec--;
                //cout << "if 1: liczbaPaczek = " << liczbaPaczek << "\n";
            }
            else if (((tmp[poczatek] + tmp[koniec]) > x) && (czyZapakowana[poczatek] == 0) && (czyZapakowana[koniec] == 0) && (tmp[koniec] != 0))
            {
                //cout << "if 2: koszulki poczatek [" << poczatek << "] = " << tmp[poczatek] << "\n";
                //cout << "if 2: koszulki koniec [" << koniec << "] = " << tmp[koniec] << "\n";
                czyZapakowana[poczatek] = 1;
                liczbaPaczek++;
                //cout << "if 2: liczbaPaczek = " << liczbaPaczek << "\n";
                poczatek++;
            }
            else if (tmp[koniec] == 0)
                koniec--;
        }
    }

    cout << liczbaPaczek << "\n";

    return 0;
}
