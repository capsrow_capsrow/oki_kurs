//https://szkopul.edu.pl/problemset/problem/koszulki/site/?key=statement
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    int x;

    cin >> n;
    cin >> x;

    int c[n];
    int k[n];
    int maxi = 0;
    long long liczbaPaczek = 0;

    for (int i = 0; i < n; ++i)
        cin >> c[i] >> k[i];

    //znajduje maxi numer kraju
    for (int i = 0; i < n; ++i)
    {
        if (k[i] > maxi)
            maxi = k[i];
    }

//    cout << "maxi = " << maxi << "\n";

    long long suma[maxi];
    for (int i = 1; i <= maxi; ++i)
        suma[i] = 0;

    for (int i = 0; i < n; ++i)
        suma[k[i]] += c[i];



    for (int j = 1; j <= maxi; ++j)
    {
        if ((suma[j] % x) == 0)
        {
            liczbaPaczek += suma[j] / x;
//            cout << "suma [" << j << "] = " << suma[j] << " --> liczba paczek = " << (suma[j] / x) << "\n";
        }
        if ((suma[j] % x) != 0)
        {
            liczbaPaczek += (suma[j] / x) + 1;
//            cout << "modulo = " << (suma[j] % x) <<"    suma [" << j << "] = " << suma[j] << " --> liczba paczek = " << (suma[j] / x) + 1 << "\n";
        }
//        cout << "liczba paczek narastajaco = " << liczbaPaczek << "\n";
    }

    cout << liczbaPaczek << "\n";

    return 0;
}
