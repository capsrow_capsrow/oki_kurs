//https://www.codingame.com/ide/puzzle/detective-pikaptcha-ep1

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int main()
{
    int width;
    int height;
    cin >> width >> height; cin.ignore();

    string tabRow[height+1];
    string sOutput[height+1];
    int iNeighbour;

    for (int i = 0; i < height; i++) {
        cin >> tabRow[i]; cin.ignore();
        cerr << "tabRow[" << i << "] = " << tabRow[i] << endl;
    }

    for (int i = 0; i < height; i++) {

        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

        sOutput[i] = "";
        //cerr << "sOutput [" << i << "] = " << sOutput << endl;
        for (int j = 0; j < width; j++)
        {
            //cerr << "i = " << i << "; j = " << j << endl;
            //cerr << "tabRow[" << i << "][" << j << "] = " << tabRow[i][j] << endl;
            if (tabRow[i][j] == '#')
            {
                sOutput[i] += "#";
                //cerr << "sOutput[" << i << "][" << j << "] = " << sOutput << endl;
                continue;
            }

            iNeighbour = 0;
            if ((tabRow[i][j-1] == '0') && ((j-1) >= 0))
                ++iNeighbour;
            if ((tabRow[i][j+1] == '0') && ((j+1) <= width))
                ++iNeighbour;
            if ((tabRow[i-1][j] == '0') && ((i-1) >= 0))
                ++iNeighbour;
            if ((tabRow[i+1][j] == '0') && ((i+1) <= height))
                ++iNeighbour;
            sOutput[i] += to_string(iNeighbour);
            //cerr << "soutput[" << i << "][" << j << "] = " << sOutput << endl;
        }
        cout << sOutput[i] << endl;
    }
}
