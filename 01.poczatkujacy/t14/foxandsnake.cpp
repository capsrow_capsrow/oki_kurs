//http://codeforces.com/problemset/problem/510/A
#include <bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n, m;
    cin >> n;
    cin >> m;

    string soutput[n+1];

    for (int i = 1; i < n + 1; ++i)
    {
        soutput[i] = "";
        if (((i % 2) == 0) && ((i % 4) != 0))
        {
            for (int j = 0; j < (m-1); ++j)
                soutput[i] += ".";
            soutput[i] += "#\n";
        }
        else if ((i % 4) == 0)
        {
            soutput[i] += "#";
            for (int j = 1; j < (m-1); ++j)
                soutput[i] += ".";
            soutput[i] += ".\n";
        }
        else
        {
            for (int j = 0; j < m; ++j)
                soutput[i] += "#";
            soutput[i] += "\n";
        }
        cout << soutput[i];
    }

    return 0;
}
