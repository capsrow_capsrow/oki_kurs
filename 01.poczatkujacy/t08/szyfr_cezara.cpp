#include <iostream>
#include <string>
using namespace std;


int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int metoda; //1 - szyfruj, 2 - odszyfruj
    int k;// krok szyfru
    string we, wy, tekst;

    cin >> tekst;
    cin >> k;
    cin.clear();
    cin.ignore(1000, '\n');
    getline(cin, we);


    if (tekst == "szyfruj")
        metoda = 1;
    else if (tekst == "odszyfruj")
        metoda = 2;


    if (metoda == 1)
    {
        //szyfrowanie
        for (int i = 0; i < we.size(); i++)
        {
            if ((we[i] >= 'a' && we[i] <= 'z') || (we[i] >= 'A' && we[i] <= 'Z'))//szyfruj tylko litery
            {
            if (we[i] >= 'a' && we[i] <= 'z' && (we[i] + k ) > 'z')
                wy += (char) ((int) 'a' + k - 1 - (int) 'z' + we[i]); // tyle dodaj do 'a'
            else if (we[i] >= 'A' && we[i] <= 'Z' && (we[i] + k ) > 'Z')
                wy += (char) ((int) 'A' + k - 1 - (int) 'Z' + we[i]);// tyle dodaj do 'A'
            else
                wy += we[i] + k;
            }
            else
                wy += we[i];
        }
    }

    if (metoda == 2)
    {
    //odszyfrowanie
        for (int i = 0; i < we.size(); i++)
        {
            if ((we[i] >= 'a' && we[i] <= 'z') || (we[i] >= 'A' && we[i] <= 'Z'))//odszyfruj tylko litery
            {
            if (we[i] >= 'a' && we[i] <= 'z' && (we[i] - k ) < 'a')
                wy += (char) ((int) 'z' - k + 1 + we[i] - (int) 'a'); // o tyle dodaj do 'a'
            else if (we[i] >= 'A' && we[i] <= 'Z' && (we[i] - k ) < 'A')
                wy += (char) ((int) 'Z' - k + 1 + we[i] - (int) 'A' );// o tyle dodaj do 'a'
            else
                wy += we[i] - k;
            }
            else
                wy += we[i];
        }
    }

    cout << wy << endl;
}
