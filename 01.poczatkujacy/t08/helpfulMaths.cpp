#include <iostream>
#include <string>
using namespace std;

int main() {

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string input_string;
    cin >> input_string;

    int no_of_1, no_of_2, no_of_3;
    no_of_1 = 0;
    no_of_2 = 0;
    no_of_3 = 0;

    for (int i = 0; i < input_string.length(); i++)
    {
        if (input_string[i] == '1')
        {
            no_of_1++;
            continue;
        }
        if (input_string[i] == '2')
        {
            no_of_2++;
            continue;
        }
        if (input_string[i] == '3')
        {
            no_of_3++;
            continue;
        }
    }

    if (no_of_1 > 0)
    {
        for (int i = 1; i <= no_of_1; i++)
        {
            if (i > 1)
                cout << '+';
            cout << '1';
        }
    }

    if (no_of_2 > 0)
    {
        if (no_of_1 > 0)
            cout << '+';

        for (int i = 1; i <= no_of_2; i++)
        {
            if (i > 1)
                cout << '+';
            cout << '2';
        }
    }

    if (no_of_3 > 0)
    {
        if (no_of_2 > 0)
            cout << '+';
        else if ((no_of_2 == 0) && (no_of_1 > 0))
            cout << '+';

        for (int i = 1; i <= no_of_3; i++)
        {
            if (i > 1)
                cout << '+';
            cout << '3';
        }
    }

}
